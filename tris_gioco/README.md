
# TRIS
**Credit**

- Design by: Kristi Nezhaj

**File**
- board crea  il tavolo di gioco
- x  crea interfaccia dell icona x
- O crea il cerchio
 - game  grafica principale con punteggio

**Spiegazione metodi**
- quando finisce il gioco viene chiamato il reset
- resetboard() viene chiamata dentro il reset per pulire la tavola
- isBoardFull() esegue controlli per capire se il gioco e finito
- switchPlayer() scambia il turno
- checkwinner() controlla chi dei due ha vinto oppure se è un pareggio, visualizzandolo poi su un alert //il metodo un po piu complicato
- 

**Osservazioni**
- L'interfaccia grafica è gradevole, le icone le ho trovate su internet e sono molto belle; l'interfaccia  e stata un po' impegnativa nel creare la board.
- gli algoritmi sono pressochè semplici tranne pochi (vedi checkwinner)

###  tris Interfaccia grafica

![foto](foto.png)




![foto2](foto2.png)
