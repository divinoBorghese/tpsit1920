# Server echo
## Kristi Nezhaj
### 5IC ITIS C.Zuccante AS 2019/2020


# Descrizione 
- Questa consegna si basa su un semplice collegamento tra un' applicazione flutter e un server che non fa altro che rimandare quello che il socket dell'app scrive.
Questo piccolo progetto mi è molto utile nella costruzione finale dell'ultimo progetto assegnatomi, mi sono esercitato con i socket e ho già un' idea su come cominciare.
# Linguaggi utilizzati
- `dart`

# Librerie
- ` dart:io`
- `package:flutter/material.dart`


#### Funzioni
- il progetto si basa su un'unica funzione che viene fatta partire quando viene premuto il bottone.
questa funzione crea il socket che scrive il contenuto della TextField e poi aspetta (listen) la risppsta da parte del server echo.