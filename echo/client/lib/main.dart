import 'dart:io';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cliente',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Cliente'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String miomex;
  String risposta;

  void sendText() {
    Socket.connect(ipController.text, 3000).then((socket) {
      print('*** Connected to: '
          '${socket.remoteAddress.address}:${socket.remotePort}');
      socket.write(textController.text);
      socket.listen((List<int> data) {
        setState(() {
          miomex = (String.fromCharCodes(data).trim());
        });
      }, onDone: () {
        print("*** Done");
        socket.destroy();
      });
    });
  }
  TextEditingController ipController = new TextEditingController(text: '192.168.1.123');
  TextEditingController textController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.black,
      ),
      body: Container(
          padding: new EdgeInsets.all(13.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextField(
                controller: ipController,
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'IP',
                ),
              ),
              TextField(
                controller: textController,
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Messaggio',
                ),
              ),
              Text(
                '$miomex',
                overflow: TextOverflow.ellipsis,
                maxLines: 5,
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 30 ),
              ),
              
            ],
          )
          ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: sendText,
        tooltip: 'Invia messaggio',
        child: Icon(Icons.sentiment_very_satisfied),
      ),
      backgroundColor: Color.fromARGB(255, 0,159,130),
    );
  }
}
