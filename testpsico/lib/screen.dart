import 'package:flutter/material.dart';

Stopwatch stopwatch = new Stopwatch();
ItemDrag sostituto;
ItemDrag sostituto1;
ItemDrag sostituto2;

class Screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => Homescreen();
}

List<ItemDrag> cubi = [
];

bool uno ;
bool due; 
bool tre;
bool corretto1; 
bool corretto2 ;
bool corretto3 ;

List<ItemDrag> cubi2 = [

];
String stringa1 = '';
String stringa2 = '';
String stringa3 = '';

List<ItemDrag> trapezio = [
  
];

class Homescreen extends State<Screen> {
  @override
  void initState() {
    super.initState();
    reset();
  }

  void reset() {
    setState(() {
      
   
    cubi = [
      ItemDrag(icon: AssetImage("assets/pic.png"), name: "1", value: "pic"),
      ItemDrag(icon: AssetImage("assets/med.png"), name: "3", value: "med"),
      ItemDrag(icon: AssetImage("assets/gran.png"), name: "2", value: "gran"),
    ];

    uno = false;
    due = false;
    tre = false;
    corretto1 = false;
    corretto2 = false;
    corretto3 = false;

    cubi2 = [
      ItemDrag(icon: AssetImage("assets/pic.png"), name: "1", value: "pic"),
      ItemDrag(icon: AssetImage("assets/med.png"), name: "3", value: "med"),
      ItemDrag(icon: AssetImage("assets/gran.png"), name: "2", value: "gran"),
    ];
    stringa1 = '';
    stringa2 = '';
    stringa3 = '';

    trapezio = [
      ItemDrag(icon: AssetImage("assets/pic2.png"), name: "1", value: "pic"),
      ItemDrag(icon: AssetImage("assets/med2.png"), name: "3", value: "med"),
      ItemDrag(icon: AssetImage("assets/gran2.png"), name: "2", value: "gran"),
    ];
    sostituto = null;
    sostituto1 = null;
    sostituto2 = null;
     });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(backgroundColor: Colors.blueGrey,
        title: Text("Test  pisco attitudinale", style: TextStyle(color: Colors.black),),),
        body:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <
                Widget>[
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: cubi.map((item) {
            return Container(
                padding: EdgeInsets.only(bottom: 100),
                alignment: Alignment.bottomCenter,
                margin: const EdgeInsets.all(8.0),
                child: Draggable<ItemDrag>(
                    data: item,
                    childWhenDragging: Container(),
                    feedback: Image(
                      image: item.icon,
                      fit: BoxFit.scaleDown,
                    ),
                    child: Image(
                      image: item.icon,
                      fit: BoxFit.scaleDown,
                    )));
          }).toList()),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          DragTarget<ItemDrag>(
            builder: (context, candidateData, rejectedData) {
              return Container(
                  child: trapezio[0].accepting
                      ? Stack(
                          fit: StackFit.loose,
                          children: <Widget>[
                            Positioned(
                              child: Image(image: trapezio[0].icon),
                            ),
                            Positioned(
                              child: Image(image: sostituto.icon),
                              bottom: 30,
                              left: 0.5,
                            ),
                          ],
                        )
                      : Image(image: trapezio[0].icon));
            },
            onAccept: (oggetto) {
              trapezio[0].accepting = true;
              stopwatch.start();
              sostituto = oggetto;
              setState(() {
                cubi.remove(oggetto);
              });
              uno = true;
              if (trapezio[0].name != oggetto.name) {
                stringa1 = oggetto.name.toString() +
                    "-->" +
                    trapezio[0].name.toString();
              }
              if (oggetto.value == trapezio[0].value) {
                corretto1 = true;
              }
              if (uno & due & tre) {
                stopwatch.stop();
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text(
                          corretto1 & corretto2 & corretto3
                              ? "GIUSTO\n" +
                                  'TEMPO=' +
                                  stopwatch.elapsed.toString()
                              : "SBAGLIATO\n" +
                                  'TEMPO=' +
                                  stopwatch.elapsed.toString(),
                        ),
                        content:
                            Text(stringa1 + '  ' + stringa2 + ' ' + stringa3),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('OK'),
                            onPressed: () {
                              Navigator.of(context).pop();
                                reset();                                
                            },
                          )
                        ],
                      );
                    });
              }
            },
          ),
          DragTarget<ItemDrag>(
            builder: (context, candidateData, rejectedData) {
              return Container(
                  child: trapezio[1].accepting
                      ? Stack(
                          fit: StackFit.loose,
                          children: <Widget>[
                            Positioned(
                              child: Image(image: trapezio[1].icon),
                            ),
                            Positioned(
                              child: Image(image: sostituto1.icon),
                              bottom: 30,
                              left: 0.9,
                            ),
                          ],
                        )
                      : Image(image: trapezio[1].icon));
            },
            onAccept: (oggetto) {
              trapezio[1].accepting = true;
              stopwatch.start();
              sostituto1 = oggetto;
              setState(() {
                cubi.remove(oggetto);
              });
              due = true;
              if (trapezio[1].name != oggetto.name) {
                stringa2 = oggetto.name.toString() +
                    "-->" +
                    trapezio[1].name.toString();
              }
              if (oggetto.value == trapezio[1].value) {
                corretto2 = true;
              }
              if (uno & due & tre) {
                stopwatch.stop();
                print('prova');
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text(
                          corretto1 & corretto2 & corretto3
                              ? "GIUSTO\n" +
                                  'TEMPO=' +
                                  stopwatch.elapsed.toString()
                              : "SBAGLIATO\n" +
                                  'TEMPO=' +
                                  stopwatch.elapsed.toString(),
                        ),
                        content:
                            Text(stringa1 + '  ' + stringa2 + '  ' + stringa3),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('OK'),
                            onPressed: () {
                              Navigator.of(context).pop();
                              reset();
                            },
                          )
                        ],
                      );
                    });
              }
            },
          ),
          DragTarget<ItemDrag>(
            builder: (context, candidateData, rejectedData) {
              return Container(
                  child: trapezio[2].accepting
                      ? Stack(
                          fit: StackFit.loose,
                          children: <Widget>[
                            Positioned(
                              child: Image(image: trapezio[2].icon),
                            ),
                            Positioned(
                              child: Image(image: sostituto2.icon),
                              bottom: 30,
                              left: 0.5,
                            ),
                          ],
                        )
                      : Image(image: trapezio[2].icon));
            },
            onAccept: (oggetto) {
              trapezio[2].accepting = true;
              stopwatch.start();
              sostituto2 = oggetto;
              setState(() {
                cubi.remove(oggetto);
              });
              tre = true;
              if (trapezio[2].name != oggetto.name) {
                stringa3 = oggetto.name.toString() +
                    "-->" +
                    trapezio[2].name.toString();
              }
              if (oggetto.value == trapezio[2].value) {
                corretto3 = true;
              }
              if (uno & due & tre) {
                stopwatch.stop();
                print('prova');
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text(
                          corretto1 & corretto2 & corretto3
                              ? "GIUSTO\n" +
                                  'TEMPO=' +
                                  stopwatch.elapsed.toString()
                              : "SBAGLIATO\n" +
                                  'TEMPO=' +
                                  stopwatch.elapsed.toString(),
                        ),
                        content:
                            Text(stringa1 + '  ' + stringa2 + '  ' + stringa3),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('OK'),
                            onPressed: () {
                              Navigator.of(context).pop();
                              reset();
                            },
                          )
                        ],
                      );
                    });
              }
            },
          ),
        ],
      ),
    ]));
  }
}

class ItemDrag {
  final String name;
  final String value;
  final AssetImage icon;
  bool accepting;

  ItemDrag({this.name, this.value, this.icon, this.accepting = false});
}
