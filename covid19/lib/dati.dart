import 'dart:convert';
import 'package:http/http.dart' as http;


Future<List<Data>> fetchDataList() async {
  final response =
      await http.get('https://corona.lmao.ninja/v2/countries');
  if (response.statusCode == 200) {
     final parsed = json.decode(response.body);
     

    return parsed.map<Data>((json) => Data.fromJson(json)).toList();
  } else {
    throw Exception('Failed to get Data');
  } 
}



class Data {
  //contains per classe DATA
  bool contains(String s){
     String paese= this.stato;
     return paese.contains(s);
  }


  final String stato;
  final int  casi;
  final int casiOdierni;
  final int morti;
  final int mortiOdierne;
  final int ricoverati;
  final int critici;
  final String bandiera;


  Data({
    this.stato, 
    this.casi, 
    this.casiOdierni,
    this.morti,
    this.mortiOdierne,
    this.ricoverati,
    this.critici,
    this.bandiera
    });
  
  
  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      stato: json['country'] as String,
      casi: json['cases'] as int,
      casiOdierni: json['todayCases'] as int,
      morti: json['deaths'] as int,
      mortiOdierne: json['todayDeaths'] as int,
      ricoverati: json['recovered'] as int,
      critici: json['critical'] as int,
      bandiera: json['countryInfo']['flag'] as String
      );
  }

}

