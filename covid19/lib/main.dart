import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'design.dart';
import 'dati.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'am029_mareee',
      theme: ThemeData(primaryColor: Colors.black),
      home: MyHomePage(title: 'am029_maree'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

TextEditingController controller = TextEditingController();
List<Data> dataList = <Data>[]; //fix erroe
var lista2 = List<Data>(); //lista 'dinamica'
var _deaths;
var _country;
var _cases;

class _MyHomePageState extends State<MyHomePage> {
  void _getData() async {
    dataList = await fetchDataList();
    setState(() {
      _country = dataList[0].stato;
      _deaths = dataList[0].morti;
      _cases = dataList[0].casi;
      lista2.addAll(dataList);
    });
  }

  @override
  void initState() {
    setState(() {
      _getData();
    });
  }

  void azioneBottone(String scelta) {
    if (scelta == Costanti.alfabetico) {
      ordineAlfabetico();
    }
    if (scelta == Costanti.decrescente) {
      ordineDecrescente();
    }
    if (scelta == Costanti.crescente) {
      ordineCrescente();
    }
  }

  void ricerca(String query) {
    //print(query);
    List<Data> dummySearchList = List<Data>();
    dummySearchList.addAll(dataList);
    if (query.isNotEmpty) {
      List<Data> dummyListData = List<Data>();
      dummySearchList.forEach((item) {
        if (item.contains(query)) {
          //print(1);
          dummyListData.add(item);
        }
      });
      setState(() {
        lista2.clear();
        lista2.addAll(dummyListData);
      });
      return;
    } else {
      //print('esce');
      setState(() {
        lista2.clear();
        lista2.addAll(dataList);
      });
    }
  }

  void ordineCrescente() {
    setState(() {
      lista2.sort((a, b) => a.casi.compareTo(b.casi));
    });
  }

  void ordineDecrescente() {
    setState(() {
      lista2.sort((a, b) => b.casi.compareTo(a.casi));
    });
  }

  void ordineAlfabetico() {
    setState(() {
      lista2.clear();
      lista2.addAll(dataList);
    });
  }

  void click(int index) {
    print(index);
    // if(index==0){
    //   Navigator.of(context).push(MaterialPageRoute(
    //       builder: (BuildContext context) => MyHomePage()));
    // }
    if (index == 1) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => WebView(
                initialUrl:
                    "https://www.google.com/search?q=notizie+coronavirus&tbm=nws&source=univ&tbo=u&sa=X&ved=2ahUKEwi9_fX_p5DpAhWhysQBHQejCaoQt8YBKAJ6BAgJEEI&biw=1745&bih=881",
              )));
    }
    if (index == 2) {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (BuildContext context) => Page2()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 25, 25, 25),
        body: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  actions: <Widget>[
                    PopupMenuButton<String>(
                      onSelected: azioneBottone,
                      itemBuilder: (BuildContext context) {
                        return Costanti.opzioni.map((String scelta) {
                          return PopupMenuItem<String>(
                            value: scelta,
                            child: Text(scelta),
                          );
                        }).toList();
                      },
                    )
                  ],
                  shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(10))),
                  expandedHeight: 200.0,
                  floating: false,
                  pinned: true,
                  forceElevated: true,
                  flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      title: Text("\n${'Info'}",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                          )),
                      background: Image.asset(
                        'assets/images/covidFotoAPPBAR.jpg',
                        fit: BoxFit.cover,
                      )),
                ),
              ];
            },
            body: Column(
              children: <Widget>[
                TextField(
                    style: new TextStyle(color: Colors.white),
                    controller: controller,
                    onChanged: (value) {
                      ricerca(value);
                    },
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                        contentPadding: EdgeInsets.all(15.0),
                        hintText: 'Ricerca un Paese',
                        hintStyle: TextStyle(color: Colors.grey))),
                Expanded(
                  child: Contenitore(),
                )
              ],
            )),
        bottomNavigationBar: BottomNavigationBar(
          onTap: click,
          backgroundColor: Colors.black,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home, color: Colors.white),
              title: Text(
                'Stati',
                style: TextStyle(color: Colors.white),
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.subtitles, color: Colors.white),
              title: Text(
                'News',
                style: TextStyle(color: Colors.white),
              ),
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.info, color: Colors.white),
                title: Text(
                  'Info',
                  style: TextStyle(color: Colors.white),
                )),
          ],
        ));
  }
}

class Page2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 25, 25, 25),
      appBar: AppBar(
        centerTitle: true,
        title: Text("Informazioni e prevenzione"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: ListView(
            children: <Widget>[
              Card(
                color: Color.fromARGB(255, 35, 35, 35),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                elevation: 7,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: 100,
                      width: 100,
                      padding: EdgeInsets.only(
                          left: 30, top: 30, bottom: 70, right: 60),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                    ),
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Sintomi comuni",
                            overflow: TextOverflow.clip,
                            style: kTitleTextstyle,
                          ),
                          Text(
                            '-febbre',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          Text(
                            '-tosse secca',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          Text(
                            '-spossatezza',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                        ],
                      ),
                    ))
                  ],
                ),
              ),
              Card(
                color: Color.fromARGB(255, 35, 35, 35),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                elevation: 7,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: 100,
                      width: 100,
                      padding: EdgeInsets.only(
                          left: 30, top: 30, bottom: 70, right: 60),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                    ),
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Sintomi rari",
                            overflow: TextOverflow.clip,
                            style: kTitleTextstyle,
                          ),
                          Text(
                            '-indolenzimento e dolori muscolari',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          Text(
                            '-mal di gola',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          Text(
                            '-congiuntivite',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          Text(
                            '-mal di testa',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                        ],
                      ),
                    ))
                  ],
                ),
              ),
              Card(
                color: Color.fromARGB(255, 35, 35, 35),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                elevation: 7,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: 100,
                      width: 100,
                      padding: EdgeInsets.only(
                          left: 30, top: 30, bottom: 70, right: 60),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                    ),
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Sintomi gravi",
                            overflow: TextOverflow.clip,
                            style: kTitleTextstyle,
                          ),
                          Text(
                            '-difficoltà respiratoria o fiato corto',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          Text(
                            '-oppressione o dolore al petto',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          Text(
                            '-perdita della facoltà di parola o di movimento',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                        ],
                      ),
                    ))
                  ],
                ),
              ),
              Card(
                color: Color.fromARGB(255, 35, 35, 35),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                elevation: 7,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: 100,
                      width: 100,
                      padding: EdgeInsets.only(
                          left: 30, top: 30, bottom: 70, right: 60),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                    ),
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Prevenzione",
                            overflow: TextOverflow.clip,
                            style: kTitleTextstyle,
                          ),
                          Text(
                            '-Lava spesso le mani con acqua e sapone o usando un prodotto disinfettante a base alcolica per le mani.',
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                          Text(""),
                          Text(
                            '-Mantieni una distanza di sicurezza da chiunque tossisca o starnutisca',
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                          Text(""),
                          Text(
                            '-Non toccarti gli occhi, il naso o la bocca.',
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                          Text(""),
                          Text(
                            '-Tossisci o starnutisci nella piega del gomito o usa un fazzoletto di carta, coprendo il naso e la bocca.',
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                        ],
                      ),
                    ))
                  ],
                ),
              ),
            ],
          ))
        ],
      ),
    );
  }
}

class Contenitore extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.all(20),
      itemCount: lista2.length,
      itemBuilder: (BuildContext context, int index) {
        var item = lista2[index];
        return Card(
          color: Color.fromARGB(255, 35, 35, 35),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          elevation: 7,
          child: Row(
            children: <Widget>[
              Container(
                height: 100,
                width: 100,
                padding:
                    EdgeInsets.only(left: 30, top: 30, bottom: 70, right: 60),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: NetworkImage(item.bandiera, scale: 0.0000000001),
                        fit: BoxFit.cover)),
              ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      item.stato,
                      overflow: TextOverflow.clip,
                      style: TextStyle(
                          color: Colors.deepOrange,
                          fontWeight: FontWeight.w700,
                          fontSize: 18),
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          'Deceduti : ' + (item.morti).toString(),
                          style: TextStyle(fontSize: 14, color: Colors.red),
                        ),
                        Text(
                          '      + ' +
                              ((item.mortiOdierne / item.morti) * 100)
                                  .toStringAsFixed(2) +
                              '%',
                          style: TextStyle(
                              fontSize: 14, color: Colors.purpleAccent),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          'Casi totali : ' + (item.casi).toString(),
                          style: TextStyle(fontSize: 14, color: Colors.white),
                        ),
                        Text(
                          '   + ' +
                              ((item.casiOdierni / item.casi) * 100)
                                  .toStringAsFixed(2) +
                              '%',
                          style: TextStyle(
                              fontSize: 14, color: Colors.purpleAccent),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          'Guarigioni : ' + (item.ricoverati).toString(),
                          style: TextStyle(
                              fontSize: 14, color: Colors.greenAccent),
                        ),
                        Text(
                          '   + ' +
                              ((item.casiOdierni / item.casi) * 100)
                                  .toStringAsFixed(2) +
                              '%',
                          style: TextStyle(
                              fontSize: 14, color: Colors.purpleAccent),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          ('Casi oggi : ' + item.casiOdierni.toString()),
                          style: TextStyle(fontSize: 14, color: Colors.orange),
                        ),
                        Text(
                          '   + ' +
                              ((item.casiOdierni / item.casi) * 100)
                                  .toStringAsFixed(2) +
                              '%',
                          style: TextStyle(
                              fontSize: 14, color: Colors.purpleAccent),
                        ),
                      ],
                    )
                  ],
                ),
              ))
            ],
          ),
        );
      },
    );
  }
}

class Costanti {
  static const String crescente = 'Crescente';
  static const String decrescente = 'Decrescente';
  static const String alfabetico = 'Alfabetico';
  static const List<String> opzioni = <String>[
    crescente,
    decrescente,
    alfabetico
  ];
}
