# Cronometro/timer Tipsit
## Kristi Nezhaj
### 5IC ITIS C.Zuccante AS 2019/2020

# Avvertenze
- Il codice può essere ancora migliorato, come per esempio la grafica(il solo unico numero del cronometro quando è<10, basterebbe un controllo), una scelta del tempo del timer(standardizzato a 60 sec).
# Descrizione 
- Questo cronometro si basa su 3 diversi widget,il primo widget e quello che crea le 2 tab, serve a controllare il widget del cronometro e il widget del timer.
Il progetto fa parte interamente di un'unica classe.
La grafica risulta basilare ed è composta da 3 semplici bottoni e un text per entrambe le pagine.
Viene invocato anche un StreamSubscriber per gestire gli eventi dello stream del timer/cronometro.
# Linguaggi utilizzati
- `dart`

# Librerie
- ` dart:async`
- `package:flutter/material.dart`


#### Funzioni
- `startCrono()` funzione chiamata sul Onpressed del bottone start,fa partire lo stream.
-` _convertStopwatchTime()` funzione che trasforma i secondi in minuti e i minuti a loro volta in ore.
- `stopCrono()` ferma lo stream.
- `resetCrono()` ferma lo stream e azzera i valori
- `Stream<int> increment(Duration interval)` funzione cuore del progetto che incrementa la variabile secondi dopo un intervallo prefissato a 1s.
- `startTimer()` funzione chiamata sul Onpressed del bottone start,fa partire lo stream.
- `_converttimerwatchTime()`funzione che controlla il tempo affinche non prenda valori negativi.
- `stopTimer()` ferma lo stream del timer
- `resettimer()` ferma lo stream del timer e resetta i valori a default
- `Stream<int> decrement(Duration interval)` decrementa il tempo del timer