import 'dart:async';
import 'dart:collection';

import 'package:flutter/material.dart';
import './crono.dart' as crono;
import './timer.dart' as timer;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Progetto Tipsit',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Progetto Tipsit'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int minuti = 0;
  int secondi = 0;
  int ore = 0;

  String timeDisplay;
  bool startIsClicked = true;
  bool stopIsClicked = true;
  bool resetIsClicked = true;
  Stream<int> flusso;
  var value;
  StreamSubscription _stopwatchStreamSubscriber;
  Stream<int> _stopwatchStream;
  void startCrono() {
    startIsClicked = false;
    _stopwatchStream = increment(Duration(seconds: 1));
    _stopwatchStreamSubscriber =
        _stopwatchStream.listen((_stopWatchSeconds) => _convertStopwatchTime());
  }

  String getMinuti() {
    if (minuti < 10) return '0$minuti';
  }

  String getsecondi() {
    if (secondi < 10) return '0$secondi';
  }

  void _convertStopwatchTime() {
    setState(() {
      if (secondi < 10) {}
      if (minuti <= 59) {
      } else {
        ore++;
        minuti = 0;
      }

      if (secondi <= 59) {
      } else {
        minuti++;
        secondi = 0;
      }
    });
  }

  void stopCrono() {
    if (startIsClicked == false) startIsClicked = true;
    setState(() {
      _stopwatchStreamSubscriber.pause();
    });
  }

  void resetCrono() {
    if (startIsClicked == false) startIsClicked = true;
    setState(() {
      _stopwatchStreamSubscriber.pause();
      minuti = 0;
      secondi = 0;
      ore = 0;
    });
  }

  Stream<int> increment(Duration interval) async* {
    while (true) {
      await Future.delayed(interval);
      yield secondi++;
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: Text(widget.title),
            bottom: TabBar(
              tabs: <Widget>[
                Tab(
                  icon:Icon(Icons.timelapse) ),
                Tab(
                  icon:Icon(Icons.timer) ),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              cronometro(),
              mytimer(),
            ],
          )),
    );
  }

  Widget cronometro() {
    return Container(
        child: Scaffold(
            backgroundColor: Colors.black,
            body: Column(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      '$ore:$minuti:$secondi',
                      style: TextStyle(
                        fontSize: 70.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(18.0),
                                  side: BorderSide(color: Colors.redAccent)),
                              onPressed: stopCrono,
                              color: Color.fromARGB(255, 160, 0, 0),
                              padding: EdgeInsets.symmetric(
                                horizontal: 40.0,
                                vertical: 15.0,
                              ),
                              child: Text(
                                "Stop",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(18.0),
                                  side: BorderSide(color: Colors.yellow)),
                              onPressed: resetCrono,
                              color: Color.fromARGB(255, 255, 255, 130),
                              padding: EdgeInsets.symmetric(
                                horizontal: 40,
                                vertical: 15.0,
                              ),
                              child: Text(
                                "Reset",
                                style: TextStyle(
                                  fontSize: 20.0,
                                  color: Colors.black,
                                ),
                              ),
                            )
                          ],
                        ),
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.white)),
                          onPressed: startIsClicked ? startCrono : () {},
                          color: Color.fromARGB(110, 66, 255, 199),
                          padding: EdgeInsets.symmetric(
                            horizontal: 80.0,
                            vertical: 20.0,
                          ),
                          child: Text(
                            "Start",
                            style: TextStyle(
                              fontSize: 24.0,
                              color: Colors.green,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            )));
  }
    bool startIsClickedtime = true;
  bool stopIsClickedtime = true;
  bool resetIsClickedtime = true;
  int secondiTime = 59;
  StreamSubscription _timerStreamSubscriber;
  Stream<int> _timerStream;
  void startTimer() {
    startIsClickedtime = false;
    _timerStream = decrement(Duration(seconds: 1));
    _timerStreamSubscriber =
        _timerStream.listen((_timerSeconds) => _converttimerwatchTime());
  }

  void _converttimerwatchTime() {
    setState(() {
      if (secondiTime == 1) {
        _timerStreamSubscriber.pause();
      }
    });
  }

  void stopTimer() {
    if (startIsClickedtime == false) startIsClickedtime = true;
    setState(() {
      _timerStreamSubscriber.pause();
    });
  }

  void resettimer() {
    if (startIsClickedtime == false) startIsClickedtime = true;
    setState(() {
      _timerStreamSubscriber.pause();
      secondiTime = 59;
    });
  }

  Stream<int> decrement(Duration interval) async* {
    while (true) {
      await Future.delayed(interval);
      yield secondiTime--;
    }
  }

  Widget mytimer() {
    return Container(
        color: Color.fromARGB(255, 203, 040, 033),
        child: Scaffold(
            backgroundColor: Colors.black,
            body: Column(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      '$secondiTime',
                      style: TextStyle(
                        fontSize: 70.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(18.0),
                                  side: BorderSide(color: Colors.redAccent)),
                              onPressed: stopTimer,
                              color: Color.fromARGB(255, 160, 0, 0),
                              padding: EdgeInsets.symmetric(
                                horizontal: 40.0,
                                vertical: 15.0,
                              ),
                              child: Text(
                                "Stop",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(18.0),
                                  side: BorderSide(color: Colors.yellow)),
                              onPressed: resettimer,
                              color: Color.fromARGB(255, 255, 255, 130),
                              padding: EdgeInsets.symmetric(
                                horizontal: 40,
                                vertical: 15.0,
                              ),
                              child: Text(
                                "Reset",
                                style: TextStyle(
                                  fontSize: 20.0,
                                  color: Colors.black,
                                ),
                              ),
                            )
                          ],
                        ),
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.white)),
                          onPressed: startIsClickedtime ? startTimer : () {},
                          color: Color.fromARGB(110, 66, 255, 199),
                          padding: EdgeInsets.symmetric(
                            horizontal: 80.0,
                            vertical: 20.0,
                          ),
                          child: Text(
                            "Start",
                            style: TextStyle(
                              fontSize: 24.0,
                              color: Colors.green,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            )));
  }
}
