# ChatRoom Tipsit
## Kristi Nezhaj
### 5IC ITIS C.Zuccante AS 2019/2020

# Avvertenze
- Il codice può essere ancora migliorato,non ci sono grossi bug.
# Riflessioni
- Ho deciso di collegare il progetto a Firebase, perchè quest' ultimo offre molti tipi di servizi e lavora molto bene con flutter 
  ci sono molti tipi di servizi come per esempio le 'notifiche push' che vorrei implementare piu avanti per rendere questo progetto 
  qualcosa di più corposo.
  Devo però ammettere di aver avuto inizialmente problemi a collegare firebase e il progetto perchè non si 'vedevano', dopo qualche tentativo 
  con pazienza sono riuscito a far in modo che si vedessero.
  Cambiando pc bisognava aggiungere anche la chiave  SHA1 del pc nuovo.
  Per esempio vorrei concentrarmi sul salvare i messaggi affinchè l' utente posssa leggere anche i messagi piu vecchi, magari con data e ora.
  Non sono ancora riuscito a creare anche un menu per le chat private con gli utenti connessi, ma ho provato a lavorarci e penso di riuscirci.
  Avevo in mente di sviluppare questo progetto anche per IOS, firebase offre metodo di accesso anche con APPLE.
# Descrizione 
- E' presente una schermata iniziale con il login Google, superata si apre una pagina iniziale che saluta l'utente e gli chiede se vuole continuare.
- appare la vera e propria stanza condivisa, in cui gli utenti si scambiano i messaggi in broadcasting.
# Linguaggi utilizzati
- `dart`
# Librerie
- ` dart:async`
-  ` dart:ui `
- `package:firebase_auth/firebase_auth.dart`
- `package:google_sign_in/google_sign_in.dart`
- `package:flutter/material.dart`

