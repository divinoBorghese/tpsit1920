import 'dart:io';
import 'dart:ui';
import 'package:chatroom/login_page.dart';
import 'package:flutter/material.dart';
import 'sign_in.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch:Colors.blue
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  MyHomePageState createState() => MyHomePageState();
}

Socket socket;
String username = name;
int cont = 0;
Future log() async {
  if (cont < 1) {
    socket = await Socket.connect("192.168.43.152", 3000);
  }
}

class MyHomePageState extends State<MyHomePage> {
  int cont = 0;
  @override
  Widget build(BuildContext context) {
    return LoginPage();
  }
}

class ChatScreen extends StatefulWidget {
  @override
  State createState() => new ChatScreenState();
}

List<ChatMessage> messaggi = <ChatMessage>[];
String s;
int i=0;
class ChatScreenState extends State<ChatScreen> {
  @override
  void initState() {
    super.initState();
    socket.listen((List<int> data) {
      s=String.fromCharCodes(data).trim();
      ChatMessage message;
      message = ChatMessage(text:s);
      setState(() {
        messaggi.insert(0, message);
      });
    });
  }

  String s;
  TextEditingController _textController = TextEditingController();
  void _handleSubmitted(String text) {
    if (_textController.text != "") {
      String s= _textController.text;
      socket.write(name+':'+_textController.text);
      _textController.clear();
      ChatMessage message;
      message = ChatMessage(text:name+':'+s);
        setState(() {
        messaggi.insert(0, message);
      });
    }
  }

  Widget _textComposerWidget() {
    return IconTheme(
      data: IconThemeData(color: Colors.blue),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          children: <Widget>[
            Flexible(
              child: new TextField(
                decoration: InputDecoration.collapsed(
                  hintText: "Invia un messaggio",
                ),
                controller: _textController,
                onSubmitted: _handleSubmitted,
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 4.0),
              child: IconButton(
                icon: new Icon(Icons.send),
                onPressed: () => _handleSubmitted(_textController.text),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('ZucChat'),
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: <Color>[
                  Colors.blue,
                  Colors.red
                ],
              ),
            ),
          ),
        ),
        body:Container(child: 
         Column(children: <Widget>[
          Flexible(
            child: ListView.builder(
              padding: EdgeInsets.all(8.0),
              reverse: true,
              itemBuilder: (_, int index) => messaggi[index],
              itemCount: messaggi.length,
            ),
          ),
          Divider(
            height: 1.0,
          ),
          Container(
            decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
            ),
            child: _textComposerWidget(),
          ),
        ]),
       
       ));
  }
}

class ChatMessage extends StatelessWidget {
  final String text;
  List <String> split=<String>['',''];
  ChatMessage({this.text,name });
  String controllo(){
  if(text.contains('There are')){
    return text;
  }
  else return text.split(':')[1];

  }
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(right: 16.0),
            child: CircleAvatar(
              child: Text(text[0]),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(text.split(':')[0], style: TextStyle(fontSize: 15)),
              Container(
                margin: const EdgeInsets.only(top: 5.0),
                child: Text(
                  controllo(),
                  style: TextStyle(fontSize: 13,
                ),
              )
              )],
          )
        ],
      ),
    );
  }
}

class PrimaSchermata extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: <Color>[
                  Colors.blue,
                  Colors.red
                ],
              ),
            ),
          ),
          title: Text("Ciao " + username + '!',style:TextStyle(color: Colors.black),)),
      body:Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.red, Colors.blue],
          ),
        ),
        child: 
      Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Bentornato! ' + name,
                style: TextStyle(
                  fontSize: 28,
                  height: 5,
                  foreground: Paint()
                    ..style = PaintingStyle.fill
                    ..strokeWidth = 1
                    ..color = Colors.white,
                ),
              ),
              RaisedButton(
                child: Text('Clicca per proseguire',style: TextStyle(color: Colors.white),),
                textColor: Colors.black,
                onPressed: () async {
                  await log();
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return ChatScreen();
                      },
                    ),
                  );
                },
                color: Colors.blue,
              )
            ]),
      ),
    ));
  }
}

